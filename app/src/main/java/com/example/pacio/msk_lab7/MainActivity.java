package com.example.pacio.msk_lab7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Tasko taks=new Tasko(new MyJsonResponseListener() {
            @Override
            public void onJsonResponseChange(String string) {
                Log.d("msk","Poszedl string: "+string);
                TextView t=findViewById(R.id.text);
                t.setText(string);
            }
        });

        taks.execute("http://jsonplaceholder.typicode.com/");
    }
}
