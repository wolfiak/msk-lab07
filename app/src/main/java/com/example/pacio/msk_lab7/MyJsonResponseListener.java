package com.example.pacio.msk_lab7;

/**
 * Created by pacio on 27.11.2017.
 */

public interface MyJsonResponseListener {
    void onJsonResponseChange(String string);
}
