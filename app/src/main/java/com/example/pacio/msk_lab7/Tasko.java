package com.example.pacio.msk_lab7;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by pacio on 27.11.2017.
 */

public class Tasko extends AsyncTask<String, String, String>{

    private MyJsonResponseListener listener;

    public Tasko(MyJsonResponseListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... strings) {
        StringBuilder response = new StringBuilder();

        try {
            URL u =new URL(strings[0]);
            HttpURLConnection con= (HttpURLConnection) u.openConnection();

           if(con.getResponseCode() == HttpURLConnection.HTTP_OK){
               BufferedReader in=new BufferedReader(new InputStreamReader(con.getInputStream()),1024);
               String line=null;
               while((line=in.readLine()) !=null){
                   response.append(line);
               }
               in.close();
               return response.toString();
           }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        listener.onJsonResponseChange(s);
    }


}
